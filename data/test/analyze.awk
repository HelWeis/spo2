Thresholds
	   Drop,   Time,    High     Low
	     1/#   sec/#
SpO2, %:       4      10      96      90
PR, BPM:       6       8      90      60
Analysis of 2021-05-15T12:06:42--2021-05-15T15:00:26, 173.8 min of duration
	 Average Events,  Spent,   Mean,  Index,     Max     Min Higher,  Lower,
		       #     min   sec/#     #/h		     min     min
SpO2, %:    93.5       5    23.5   281.8     1.7      98      90     3.2     0.0
PR, BPM:    67.1       0     0.0     0.0     0.0     112      55     3.5     5.6
 SpO2, %   Time,   Time,
   below     min       %
      95   141.2    81.3
      90     0.0     0.0
      85     0.0     0.0
      80     0.0     0.0
      75     0.0     0.0
      70     0.0     0.0
      65     0.0     0.0
      60     0.0     0.0
      55     0.0     0.0
      50     0.0     0.0
